package pl.edu.ug.servletcalc;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class CalcServlet extends HttpServlet {

    private ArrayList<String> calculation;
    private ArrayList<String> history;

    CalcServlet(){
        super();
        calculation = new ArrayList<>();
        history = new ArrayList<>();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        print(resp, " ");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter out  = resp.getWriter();

        resp.setContentType("text/html");

        calculation.add(req.getParameter("input"));
        String operator = req.getParameter("operator");
        calculation.add(operator);

        if(operator.equals("=")){
            StringBuilder operation = new StringBuilder();
            for(String s : calculation){
                operation.append(s);
            }

            double result = 0;

            try {
                result = Double.parseDouble(calculation.get(0));
            } catch (ArithmeticException e){
                out.println("Bad format");
            }


            for(int i = 1; i < calculation.size(); i+=2){
                operator = calculation.get(i);
                if(operator.equals("=")){
                    break;
                }
                int number = Integer.parseInt(calculation.get(i+1));

                switch (operator) {
                    case "+":
                        result += number;
                        break;
                    case "-":
                        result -= number;
                        break;
                    case "*":
                        result *= number;
                        break;
                    case "/":
                        result /= number;
                        break;
                }
            }
            operation.append(result);
            history.add(operation.toString());
            print(resp, String.valueOf(result));
            calculation.clear();
        } else {
            print(resp, " ");
        }
    }

    private void print(HttpServletResponse resp, String result) throws IOException {
        PrintWriter out  = resp.getWriter();

        resp.setContentType("text/html");

        out.println("<html><body>");
        out.println("Result: " + result);
        out.println("<form action='/calc' method='POST'>");
        out.println("<input type='text' name='input' />");
        out.println("<input type='submit' name='operator' value='+' />");
        out.println("<input type='submit' name='operator' value='-' />");
        out.println("<input type='submit' name='operator' value='*' />");
        out.println("<input type='submit' name='operator' value='/' />");
        out.println("<input type='submit' name='operator' value='=' />");
        out.println("</form>");
        for(String s : history){
            out.println(s);
            out.println("<br />");
        }
        out.println("</body></html>");
    }
}
