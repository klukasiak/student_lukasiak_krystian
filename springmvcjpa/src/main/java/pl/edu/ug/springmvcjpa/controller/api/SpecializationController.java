package pl.edu.ug.springmvcjpa.controller.api;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.edu.ug.springmvcjpa.domain.Specialization;
import pl.edu.ug.springmvcjpa.service.SpecializationService;

import java.util.List;

@RestController
@RequestMapping("/api/specialization")
@RequiredArgsConstructor
public class SpecializationController {

    private final SpecializationService specializationService;

    @GetMapping
    public List<Specialization> getSpecializations() {
        return specializationService.getAll();
    }

    @GetMapping("/{id}")
    public Specialization getSpecialization(@PathVariable Long id) {
        return specializationService.getById(id);
    }

    @PostMapping
    public Specialization addSpecialization(@RequestBody Specialization specialization) {
        return specializationService.add(specialization);
    }

    @PostMapping("/all")
    public List<Specialization> addAllSpecializations(@RequestBody List<Specialization> specializations) {
        return specializationService.addAll(specializations);
    }

    @DeleteMapping("/{id}")
    public void deleteSpecialization(@PathVariable Long id) {
        specializationService.delete(id);
    }

    @PutMapping("/{id}")
    public Specialization updateSpecialization(@RequestBody Specialization specialization, @PathVariable Long id) {
        return specializationService.update(id, specialization);
    }
}
