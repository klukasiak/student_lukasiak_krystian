package pl.edu.ug.springmvcjpa.controller.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.ug.springmvcjpa.domain.Doctor;
import pl.edu.ug.springmvcjpa.service.DoctorService;
import pl.edu.ug.springmvcjpa.service.SpecializationService;

@Controller("doctorweb")
@RequestMapping("/doctor")
@RequiredArgsConstructor
public class DoctorController {

    private final DoctorService doctorService;
    private final SpecializationService specializationService;

    @GetMapping
    public String homeDoctor(Model model) {
        model.addAttribute("doctors", doctorService.getAll());
        return "doctor-home";
    }

    @GetMapping("/form/{id}")
    public String formDoctor(@PathVariable Long id, Model model) {
        if (id == 0) {
            model.addAttribute("fieldDisabled", "disabled");
        }
        model.addAttribute("doctor", doctorService.getById(id));
        model.addAttribute("specializations", specializationService.getAll());
        return "doctor-form";
    }

    @PostMapping("/add")
    public String addDoctor(Doctor doctor, Model model) {
        doctorService.add(doctor);
        model.addAttribute("doctors", doctorService.getAll());
        return "doctor-home";
    }

    @GetMapping("/delete/{id}")
    public String deleteDoctor(@PathVariable Long id, Model model) {
        doctorService.delete(id);
        model.addAttribute("doctors", doctorService.getAll());
        return "doctor-home";
    }
}
