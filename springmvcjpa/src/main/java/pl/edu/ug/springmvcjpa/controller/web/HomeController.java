package pl.edu.ug.springmvcjpa.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller("homeweb")
public class HomeController {
    @GetMapping("/")
    public String home(Model model) {
        return "home";
    }
}
