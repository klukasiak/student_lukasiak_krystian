package pl.edu.ug.springmvcjpa.controller.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.ug.springmvcjpa.domain.Patient;
import pl.edu.ug.springmvcjpa.service.PatientService;

@Controller("patientweb")
@RequestMapping("/patient")
@RequiredArgsConstructor
public class PatientController {

    private final PatientService patientService;

    @GetMapping
    public String homePatient(Model model) {
        model.addAttribute("patients", patientService.getAll());
        return "patient-home";
    }

    @GetMapping("/form/{id}")
    public String formPatient(@PathVariable Long id, Model model) {
        if (id == 0) {
            model.addAttribute("fieldDisabled", "disabled");
        }
        model.addAttribute("patient", patientService.getById(id));
        return "patient-form";
    }

    @PostMapping("/add")
    public String addPatient(Patient patient, Model model) {
        patientService.add(patient);
        model.addAttribute("patients", patientService.getAll());
        return "patient-home";
    }

    @GetMapping("/delete/{id}")
    public String deletePatient(@PathVariable Long id, Model model) {
        patientService.delete(id);
        model.addAttribute("patients", patientService.getAll());
        return "patient-home";
    }
}
