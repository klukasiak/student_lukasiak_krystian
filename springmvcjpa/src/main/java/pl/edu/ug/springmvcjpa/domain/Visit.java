package pl.edu.ug.springmvcjpa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_patient")
    private Patient patient;

    @ManyToMany
    @JoinTable(
            name = "DOCTOR_VISIT",
            joinColumns = @JoinColumn(name = "id_visit"),
            inverseJoinColumns = @JoinColumn(name = "id_doctor")
    )
    @JsonIgnoreProperties("visits")
    private List<Doctor> doctors;
}
