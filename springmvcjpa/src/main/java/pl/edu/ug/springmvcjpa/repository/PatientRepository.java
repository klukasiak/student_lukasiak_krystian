package pl.edu.ug.springmvcjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ug.springmvcjpa.domain.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
}
