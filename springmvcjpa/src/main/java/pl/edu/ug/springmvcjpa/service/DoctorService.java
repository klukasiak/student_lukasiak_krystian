package pl.edu.ug.springmvcjpa.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.ug.springmvcjpa.domain.Doctor;
import pl.edu.ug.springmvcjpa.repository.DoctorRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    public List<Doctor> getAll() {
        return doctorRepository.findAll();
    }

    public Doctor getById(Long id) {
        return doctorRepository.findById(id).orElse(new Doctor());
    }

    public Doctor add(Doctor doctor) {
        return doctorRepository.save(doctor);
    }

    public List<Doctor> addAll(List<Doctor> doctors) {
        return doctorRepository.saveAll(doctors);
    }

    public Doctor update(Long id, Doctor doctor) {
        doctor.setId(id);
        return doctorRepository.save(doctor);
    }

    public void delete(Long id) {
        if (getById(id) != null) {
            doctorRepository.deleteById(id);
        }
    }
}
