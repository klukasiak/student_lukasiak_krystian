package pl.edu.ug.springmvcjpa.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.ug.springmvcjpa.domain.Patient;
import pl.edu.ug.springmvcjpa.repository.PatientRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PatientService {

    private final PatientRepository patientRepository;

    public List<Patient> getAll() {
        return patientRepository.findAll();
    }

    public Patient getById(Long id) {
        return patientRepository.findById(id).orElse(new Patient());
    }

    public Patient add(Patient patient) {
        return patientRepository.save(patient);
    }

    public List<Patient> addAll(List<Patient> patients) {
        return patientRepository.saveAll(patients);
    }

    public Patient update(Long id, Patient patient) {
        patient.setId(id);
        return patientRepository.save(patient);
    }

    public void delete(Long id) {
        if (getById(id) != null) {
            patientRepository.deleteById(id);
        }
    }
}
