package pl.edu.ug.springmvcjpa.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.edu.ug.springmvcjpa.domain.Specialization;
import pl.edu.ug.springmvcjpa.repository.SpecializationRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class SpecializationService {

    private final SpecializationRepository specializationRepository;

    public List<Specialization> getAll() {
        return specializationRepository.findAll();
    }

    public Specialization getById(Long id) {
        return specializationRepository.findById(id).orElse(new Specialization());
    }

    public Specialization add(Specialization specialization) {
        return specializationRepository.save(specialization);
    }

    public List<Specialization> addAll(List<Specialization> specializations) {
        return specializationRepository.saveAll(specializations);
    }

    public Specialization update(Long id, Specialization specialization) {
        specialization.setId(id);
        return specializationRepository.save(specialization);
    }

    public void delete(Long id) {
        if (getById(id) != null) {
            specializationRepository.deleteById(id);
        }
    }
}
