package pl.edu.ug.springmvcwarehouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringmvcwarehouseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringmvcwarehouseApplication.class, args);
    }

}
