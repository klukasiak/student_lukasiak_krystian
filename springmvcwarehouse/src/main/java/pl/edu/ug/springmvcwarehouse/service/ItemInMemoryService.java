package pl.edu.ug.springmvcwarehouse.service;

import org.springframework.stereotype.Service;
import pl.edu.ug.springmvcwarehouse.domain.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ItemInMemoryService implements ItemManager {
    private static List<Item> items = new ArrayList<>();

    @Override
    public void addItem(Item item) {
        item.setId(UUID.randomUUID().toString());
        items.add(item);
    }

    @Override
    public Item findById(String id) {
        for (Item i : items) {
            if (i.getId().equals(id)) {
                return i;
            }
        }
        return null;
    }

    @Override
    public List<Item> getAllItems() {
        return items;
    }

    @Override
    public void deleteById(String id) {
        Item toRemove = findById(id);
        if (toRemove != null) {
            items.remove(toRemove);
        }
    }

    @Override
    public Item updateItem(String id, Item item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(id)) {
                items.get(i).setWeight(item.getWeight());
                items.get(i).setValue(item.getValue());
                items.get(i).setShelf(item.getShelf());
                items.get(i).setPosition(item.getPosition());
                items.get(i).setName(item.getName());
                return items.get(i);
            }
        }
        return null;
    }
}
