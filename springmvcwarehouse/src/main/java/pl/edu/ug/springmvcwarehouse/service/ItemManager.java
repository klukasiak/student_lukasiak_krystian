package pl.edu.ug.springmvcwarehouse.service;

import pl.edu.ug.springmvcwarehouse.domain.Item;

import java.util.List;

public interface ItemManager {

    void addItem(Item item);

    Item findById(String id);

    List<Item> getAllItems();

    void deleteById(String id);

    Item updateItem(String id, Item item);
}
