package pl.edu.ug.springmvcwarehouse.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NoShelfValidator.class)
public @interface NoShelfNoPosition {
    String message() default "If shelf is 0, then position must be 0.";

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
