package pl.edu.ug.springreactor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringreactorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringreactorApplication.class, args);
    }

}
