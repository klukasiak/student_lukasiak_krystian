package pl.edu.ug.springreactor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ug.springreactor.domain.Patient;
import pl.edu.ug.springreactor.repository.PatientRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class PatientController {

    @Autowired
    private PatientRepository patientRepository;

    @PostMapping("/patient")
    public Mono<ResponseEntity<Patient>> addPatient(@RequestBody Patient patient) {
        return patientRepository.save(patient).map(ResponseEntity::ok);
    }

    @GetMapping("/patient")
    public Flux<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    @GetMapping("/patient/{id}")
    public Mono<Patient> getPatient(@PathVariable String id) {
        return patientRepository.findById(id);
    }

    @PutMapping("/patient/{id}")
    public Mono<ResponseEntity<Patient>> updatePatient(@PathVariable String id, @RequestBody Patient patient) {
        return patientRepository
                .findById(id)
                .flatMap(findedPatient -> {
                    findedPatient.setName(patient.getName());
                    return patientRepository.save(findedPatient);
                })
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }

    @DeleteMapping("/patient/{id}")
    public Mono<ResponseEntity<Void>> deletePatient(@PathVariable String id) {
        return patientRepository
                .findById(id)
                .flatMap(patientRepository::delete)
                .then(Mono.just(ResponseEntity.ok().<Void>build())
                .defaultIfEmpty(ResponseEntity.notFound().build()));
    }
}
