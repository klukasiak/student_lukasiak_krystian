package pl.edu.ug.springreactor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.ug.springreactor.domain.Visit;
import pl.edu.ug.springreactor.repository.VisitRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class VisitController {

    @Autowired
    private VisitRepository visitRepository;

    @PostMapping("/visit")
    public Mono<ResponseEntity<Visit>> addVisit(@RequestBody Visit visit) {
        return visitRepository.save(visit).map(ResponseEntity::ok);
    }

    @GetMapping("/visit")
    public Flux<Visit> getAllVisits() {
        return visitRepository.findAll();
    }

    @GetMapping("/visit/{id}")
    public Mono<Visit> getVisit(@PathVariable String id) {
        return visitRepository.findById(id);
    }

    @PutMapping("/visit/{id}")
    public Mono<ResponseEntity<Visit>> updateVisit(@PathVariable String id, @RequestBody Visit visit) {
        return visitRepository
                .findById(id)
                .flatMap(findedVisit -> {
                    findedVisit.setType(visit.getType());
                    return visitRepository.save(findedVisit);
                })
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }

    @DeleteMapping("/visit/{id}")
    public Mono<ResponseEntity<Void>> deleteVisit(@PathVariable String id) {
        return visitRepository
                .findById(id)
                .flatMap(visitRepository::delete)
                .then(Mono.just(ResponseEntity.ok().<Void>build()))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
