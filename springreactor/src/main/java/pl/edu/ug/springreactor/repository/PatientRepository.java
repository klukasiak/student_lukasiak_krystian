package pl.edu.ug.springreactor.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import pl.edu.ug.springreactor.domain.Patient;

public interface PatientRepository extends ReactiveMongoRepository<Patient, String> {
}
